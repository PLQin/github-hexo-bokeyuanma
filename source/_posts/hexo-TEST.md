
title: 新的开始
permalink: abc
author: Qing
tags:
- 我的
categories:
- 日志

thumbnail: https://ooo.0o0.ooo/2017/07/01/5957944f1e84d.jpg
blogexcerpt:  " 创建一个新的帖子, 一些基本的语法 "

---

这是**新的开始**，我用hexo创建了第一篇文章。

通过下面的命令，就可以创建新文章
```{bash}
D:\workspace\javascript\nodejs-hexo>hexo new 新的开始
[info] File created at D:\workspace\javascript\nodejs-hexo\source\_posts\新的开始.md
```

感觉非常好。

## 链接

-	官网中文:	https://hexo.io/zh-cn/docs/index.html
-	官网模板:	https://hexo.io/themes/

## 引用
{% blockquote Seth Godin http://sethgodin.typepad.com/seths_blog/2009/07/welcome-to-island-marketing.html Welcome to Island Marketing %}
Every interaction is both precious and an opportunity to delight.
{% endblockquote %}

## 代码块
{% codeblock .compact http://underscorejs.org/#compact Underscore.js %}
.compact([0, 1, false, 2, ‘’, 3]);
=> [1, 2, 3]
{% endcodeblock %}

## 链接
{% link 百度  http://baidu.com 百度 %}

## 图片
{% img https://ooo.0o0.ooo/2017/06/27/59522fa7908d2.gif 200 200 这是一张图片 %}


## 使用技巧

-	 Front-matter 中 ``` date: 2014-05-07 18:44:12 ``` 可以自定义日期