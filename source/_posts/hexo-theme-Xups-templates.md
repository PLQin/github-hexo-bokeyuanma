---
title: HEMO搭建
date: 
author: Qing
tags:
	-	HEXO
categories:
    - 	BLOG
thumbnail: //ooo.0o0.ooo/2017/07/01/5957944f1e84d.jpg
blogexcerpt:  "论: 如何搭建hexo-theme-xups主题的HEMO-Blog	"
---


## 文章规范

- 使用markdown写博文
- 博文图片统一位置：七牛云存储
- 标准配图
	- xups主题现默认有0-9共10张博客配图
	- 博客封面配图：200x140，命名：xxx_thumbnail
- 指明文章的标题、作者信息、封面图片地址、博客摘要
```
---
title: {{ title }}
date: {{ date }}
author:
tags:
categories:
    - Web技术
    - 生活琐事
thumbnail:
blogexcerpt:

---

```
	-	利用`<!-- more -->`或者`post.blogexcerpt`设置文章的摘要(blogexcerpt)

