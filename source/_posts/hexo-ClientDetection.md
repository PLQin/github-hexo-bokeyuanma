---
title: JavaScript の 客户端检测
# date: 2017-06-29 18:44:12
author: Qing
tags:
	-	JavaScript
categories:
	-	BLOG
blogexcerpt:  "使用 模块增强模式进行客户端检测, 常用的检测方式为: 1 能力检测  ; 2 用户代理检测; 含有检测插件, 检测浏览器引擎 :, 检测浏览器品牌　,以及网友的检测方法。 "
---

#模块增强模式进行客户端检测

常用的检测方式为:
* 	1 能力检测
*  	2 用户代理检测

这里有 2 用户代理检测

> 检测插件 P211 : 

```
非IE浏览器: 
navigator.plugins是一个包含浏览器插件的数组, 这个数组的每一项都包含:
    name : 插件的名字.
    description: 插件的描述
    filename: 插件的文件名
    length: 插件所处理的MIME类型数量

		// 检测插件(在IE中无效):
		function hasPlugin(name){
			name = name.toLowerCase(); //toLowerCase() 方法用于把字符串转换为小写
			for(var i=0;i<navigator.plugins.length; i++){
				if(navigator.plugins[i].name.toLowerCase().indexOf(name)>-1){ // >-1表示找到
					return true
				}
			}
			return false;
		};
			
		// 检测Flash
		alert(hasPlugin("Flash"));
		
		// 检测QuickTime
		alert(hasPlugin("QuickTime"));
			

IE浏览器
		// 检测IE的插件
		//  ========== 
		//  = IE以COM对象的方式实现插件,而COM对象使用唯一标示符来标识,因此要检测插件必须知道其 = 
		//  = COM标识符,例如,Flash的标识符是ShockwaveFlash.ShockwaveFlash = 
		//  ========== 
		function hasIEPlugin(name){
			try{
				new ActiveXOBject(name);
				return true;
			}catch(e){
				return false;
				//TODO handle the exception
			}
		};
		
		// IE中检测Flash
		alert(hasIEPlugin("ShockwaveFlash.ShockwaveFlash"));


在所有浏览器中检测
		// 结合之前的2个检测函数,检测所有的浏览器中的FLash
		function hasFLash(){
			var result = hasPlugin("Flash");
			if(!result){
				result.hasIEPlugin("ShockwaveFlash.ShockwaveFlash");
			}
			return result; // true or false
		};
		
		function hasQuickTime(){
			var result = hasPlugin("QuickTime");
			if (!result) {
				result = hasIEPlugin("QuickTime.Quicktime");
			} 
			return result;
		};

```



> 检测浏览器引擎 :

```

			var client = function (){
				var engine = {
					// 呈现引擎
					ie:0, // IE浏览器内核
					gecko:0, // Firefox浏览器内核	
					webkit:0, // google->chrome和apple->safari内核
					khtml:0, // Konqueror(linux平台中的一个浏览器)的内核->KHTML
					opera:0, // Opera
					
					// 具体的版本号
					ver : null
				};
				
				// 检测呈现引擎, 平台, 设备
				return {
					engine : engine
				};
			}();
			
			
			//  ====================== 
			//  = 检测客户端=>识别呈现引擎 = 
			//  ======================
			var ua = navigator.userAgent;
			console.log(ua);
			//  = 第一步先检测Opera = 
			if(window.opera){ // 如果是Opera浏览器, 则获得浏览器版本
				client.ver = window.opera.version(); // window.opera在Opera 5.0及更高版本中存在,在Opera 7.6及更高版本中,调用version()方法可以放回一个表示浏览器版本的字符串
				client.opera = parseFloat(client.ver);
			}else if(/AppleWebKit\/(\S+)/.test(ua)){
				//  = 第二步检测Webkit = 
				// webkit的用户代理字符串中的"AppleWebkit"是独一无二的, 所以检测这个字符串
				// \S : 表示非空格的特殊字符
				client.ver = RegExp["$1"];
				client.webkit = parseFloat(client.ver);
			}else if(/KHTML\/(\S+)/.test(ua) || /Konqueror\/([^;]+)/.test(ua)){
				//  = 第三步检测KHTML = 
				// 在Konqueror 3.1及更早版本中不包括KHTML的版本, 故而使用Konqueror的版本来代替
				client.ver = RegExp["$1"];
				client.khtml = parseFloat(client.ver)				
			}else if(/rv:([^\)]+)\) Gecko\/\d{8}/.test(ua)){
				//  = 第四步检测Gecko = 
				// "Gecko" 会出现在字符串"rv:"之后 ;字符串"rv:"在前面;
				client.ver = RegExp["$1"];
				client.khtml = parseFloat(client.ver)
			}else if(/MSIE ([^;]+)/.test(ua)){
				//  = 第五步检测IE = 
				client.ver = RegExp["$1"];
				client.ie = parseFloat(client.ver)
			}
			
			
			// 执行代码
			if(client.ie){ // 如果是IE呈现引擎, client.ie应该大于0
				// 针对IE的代码
				alert("我是IE ie引擎");
			}else if (client.gecko > 1.5){ // 如果是gecko呈现引擎(firefox)
				if(client.ver == "1.8.1"){
					// 针对这版本进行操作					
				}
				alert("我是firefox gecko引擎");
			}else if(client.webkit){
				alert("我是chrome webkie引擎");
			}else if(client.khtml){
				alert("我是KHTML 引擎");
			}else if(client.opera){
				alert("我是opera引擎");
			}



```

> 检测浏览器品牌　:

```
			var client = function (){
				var engine = {
					// 呈现引擎
					ie:0, // IE浏览器内核
					gecko:0, // Firefox浏览器内核	
					webkit:0, // google->chrome和apple->safari内核
					khtml:0, // Konqueror(linux平台中的一个浏览器)的内核->KHTML
					opera:0, // Opera
					
					// 具体的版本号
					ver : null
				};
				
				var browser = {
					//浏览器
					ie : 0,
					firefox:0,
					safari:0,
					honq:0,
					opera:0, 
					chrome:0,
					
					// 具体的版本号
					ver : null
				};
				
				
				// 检测呈现引擎, 平台, 设备
				return {
					engine : engine,
					browser : browser
				};
			}();
			
			
			//  ====================== 
			//  = 检测客户端=>识别呈现引擎 = 
			//  = 检测客户端=>识别浏览器 =  
			//  ====================== 
			
			var ua = navigator.userAgent; 
			//  = 第一步先检测Opera =  
			var o = window.opera || window.opr

			if(o){ // 如果是Opera浏览器, 则获得浏览器版本
				client.engine.ver = client.browser.ver = (/OPR\/(.*)/.exec(ua))[1]; // window.opera在Opera 5.0及更高版本中存在,在Opera 7.6及更高版本中,调用version()方法可以放回一个表示浏览器版本的字符串
				client.engine.opera = client.browser.opera = parseFloat(client.engine.ver);
			}else if(/AppleWebKit\/(\S+)/.test(ua)){
				//  = 第二步检测Webkit = 
				// webkit的用户代理字符串中的"AppleWebkit"是独一无二的, 所以检测这个字符串
				// \S : 表示非空格的字符
				client.engine.ver = RegExp["$1"];
				client.engine.webkit = parseFloat(client.engine.ver);

				//确定是Chrome 还是 Safari
				if(/Chrome\/(\S+)/.test(ua)){
					client.browser.ver  = RegExp["$1"];
					client.browser.chrome = parseFloat(client.browser.ver);
				}else if(/Version\/(\S+)/.test(ua)){
					client.browser.ver  = RegExp["$1"];
					client.browser.safari = parseFloat(client.browser.ver);
				}else{
					//近似的确定版本号
					var safariVersion = 1;
					if(client.webkit < 100){
						safariVersion = 1;
					}else if(client.webkit < 312){
						safariVersion = 1.2;
					}else if(client.webkit < 412){
						safariVersion = 1.3;
					}else{
						safariVersion = 2;
					}
					client.browser.safari = client.browser.ver = safariVersion;
				}
			}else if(/KHTML\/(\S+)/.test(ua) || /Konqueror\/([^;]+)/.test(ua)){
				//  = 第三步检测KHTML = 
				// 在Konqueror 3.1及更早版本中不包括KHTML的版本, 故而使用Konqueror的版本来代替
				client.engine.ver = client.browser.ver = RegExp["$1"];
				client.engine.khtml = client.browser.konq = parseFloat(client.engine.ver)
			}else if(/rv:([^\)]+)\) Gecko\/\d{8}/.test(ua)){
				//  = 第四步检测Gecko = 
				// "Gecko" 会出现在字符串"rv:"之后 ;字符串"rv:"在前面;
				client.engine.ver = RegExp["$1"];
				client.engine.gecko = parseFloat(client.engine.ver);
				
				//确定是不是firefox
				if(/Firefox\/(\S+)/.test(ua)){
					client.browser.ver = RegExp["$1"];
					client.browser.firefox = parseFloat(client.browser.ver);
				}
			}else if(/MSIE ([^;]+)/.test(ua)){
				//  = 第五步检测IE = 
				client.engine.ver = client.browser.ver = RegExp["$1"];
				client.engine.ie = client.browser.ie = parseFloat(client.engine.ver)
			}
			
			
			// 检测引擎逻辑代码
			if(client.engine.ie){ // 如果是IE呈现引擎, client.engine.ie应该大于0
				// 针对IE的代码
				alert("我是IE ie引擎");
			}else if (client.engine.gecko > 1.5){ // 如果是gecko呈现引擎(firefox)
				if(client.engine.ver == "1.8.1"){
					// 针对这版本进行操作					
				}
				alert("我是firefox gecko引擎");
			}else if(client.engine.webkit){
				alert("我是webkie引擎");
			}else if(client.engine.khtml){
				alert("我是KHTML 引擎");
			}else if(client.engine.opera){
				alert("我是opera引擎");
			}
			
			// 检测浏览器逻辑代码
			if(client.engine.webkit){ // 如果为webkit引擎
				if(client.browser.chrome){
					alert("我是google chrome")
				}else if(client.browser.safari){
					alert("我是Safari")
				}
			}else if(client.engine.gecko){
				if(client.browser.firefox){
					alert("我是火狐")
				}else{
					alert("gecko.....")
				}
			}else if(client.browser.ie){
				alert("我是IE")
			}else if(client.browser.opera){
				alert("我是Opera")
			}
			
            ```



## 用户代理检测 总结
* 这种方法对用户代理字符串有很大的依赖性, 不推荐

```
			// 模块增强模式进行客户端检测
			var client = function() {
				var engine = {
					// 呈现引擎
					ie: 0, // IE浏览器内核
					gecko: 0, // Firefox浏览器内核	
					webkit: 0, // google->chrome和apple->safari内核
					khtml: 0, // Konqueror(linux平台中的一个浏览器)的内核->KHTML
					opera: 0, // Opera

					// 具体的版本号
					ver: null
				};

				var browser = {
					// 浏览器
					ie: 0,
					firefox: 0,
					safari: 0,
					konq: 0,
					opera: 0,
					chrome: 0,

					// 具体的版本号
					ver: null
				};

				var system = {
					// 平台, 设备和操作系统
					win: false,
					mac: false,
					x11: false, // 数字1

					// 移动设备
					iphone: false,
					ipod: false,
					ipad: false,
					ios: false,
					android: false,
					nokiaN: false, // 诺基亚N系列
					winMobile: false, // window phone系列

					// 游戏系统
					wii: false, // 任天堂
					ps: false
				}

				// 检测呈现引擎和浏览器
				var ua = navigator.userAgent;
				//  = 第一步先检测Opera =  
				var o = window.opera || window.opr;
				if(o) { // 如果是Opera浏览器, 则获得浏览器版本
					engine.ver = browser.ver = (/OPR\/(.*)/.exec(ua))[1]; // window.opera在Opera 5.0及更高版本中存在,在Opera 7.6及更高版本中,调用version()方法可以放回一个表示浏览器版本的字符串
					engine.opera = browser.opera = parseFloat(engine.ver);
				} else if(/AppleWebKit\/(\S+)/.test(ua)) {
					//  = 第二步检测Webkit = 
					// webkit的用户代理字符串中的"AppleWebkit"是独一无二的, 所以检测这个字符串
					// \S : 表示非空格的字符
					engine.ver = RegExp["$1"];
					engine.webkit = parseFloat(engine.ver);

					//确定是Chrome 还是 Safari
					if(/Chrome\/(\S+)/.test(ua)) {
						browser.ver = RegExp["$1"];
						browser.chrome = parseFloat(browser.ver);
					} else if(/Version\/(\S+)/.test(ua)) {
						browser.ver = RegExp["$1"];
						browser.safari = parseFloat(browser.ver);
					} else {
						//近似的确定版本号
						var safariVersion = 1;
						if(engine.webkit < 100) {
							safariVersion = 1;
						} else if(engine.webkit < 312) {
							safariVersion = 1.2;
						} else if(engine.webkit < 412) {
							safariVersion = 1.3;
						} else {
							safariVersion = 2;
						}
						browser.safari = browser.ver = safariVersion;
					}
				} else if(/KHTML\/(\S+)/.test(ua) || /Konqueror\/([^;]+)/.test(ua)) {
					//  = 第三步检测KHTML = 
					// 在Konqueror 3.1及更早版本中不包括KHTML的版本, 故而使用Konqueror的版本来代替
					engine.ver = browser.ver = RegExp["$1"];
					engine.khtml = browser.konq = parseFloat(engine.ver)
				} else if(/rv:([^\)]+)\) Gecko\/\d{8}/.test(ua)) {
					//  = 第四步检测Gecko = 
					// "Gecko" 会出现在字符串"rv:"之后 ;字符串"rv:"在前面;
					engine.ver = RegExp["$1"];
					engine.gecko = parseFloat(engine.ver);

					//确定是不是firefox
					if(/Firefox\/(\S+)/.test(ua)) {
						browser.ver = RegExp["$1"];
						browser.firefox = parseFloat(browser.ver);
					}
				} else if(/MSIE ([^;]+)/.test(ua)) {
					//  = 第五步检测IE = 
					engine.ver = browser.ver = RegExp["$1"];
					engine.ie = browser.ie = parseFloat(engine.ver)
				}

				// 检测平台
				var p = navigator.platform; // 可能出现的值:Win32, Win64, MacPPC, MacIntel, X11, Linux i686;
				// 检测前缀, 向前兼容
				system.win = p.indexOf("Win") == 0;
				system.mac = p.indexOf("Mac") == 0;
				system.x11 = (p.indexOf("X11") == 0) || (p.indexOf("Linux") == 0);

				// 检测Windows 操作系统
				if(system.win) {
					if(/Win(?:dows )?([^do]{2})\s?(\d+\.\d+)?/.test(ua)) {
						if(RegExp["$1"] == "NT") {
							switch(RegExp["$2"]) {
								case "5.0":
									system.win = "2000";
									break;
								case "5.1":
									system.win = "Xp";
									break;
								case "6.0":
									system.win = "Vista";
									break;
								case "6.1":
									system.win = "7";
									break;
								default:
									system.win = "NT";
									break;
							}
						} else if(RegExp["$1"] == "9x") {
							system.win = "ME";
						} else {
							system.win = RegExp["$1"];
						}
					}
				}
				
				// 检测移动设备
				system.iphone = ua.indexOf("iPone") > -1;
				system.ipod = ua.indexOf("iPod") > -1;
				system.ipad = ua.indexOf("iPad") > -1;
				system.nokiaN = ua.indexOf("NokiaN") > -1;
				// windows mobile设备, 又称windows CE
				if(system.win == "CE"){ // 老版本的windows Mobile
					system.winMobile = system.win;
				}else if(system.win == "Ph"){
					if(/Window Phone OS (\d+.\d+)/.test(ua)){ // windows phone7或更新
						system.win = "Phone";
						system.winMobile = parseFloat(RegExp["$1"]);
					}
				}
				// 检测IOS版本
				if(system.mac && ua.indexOf("Mobile") > -1){
					if(/CPU (?:iPhone )?OS (\d+_\d+)/.test(ua)){
						system.ios = parseFloat(RegExp.$1.replace("_". "."));
					}else{
						system.ios = 2; // 无法检测, 推测为低版本
					}
				}
				// 检测Android 版本
				if(/Android (\d+\.\d+)/.test(ua)){
					system.android = parseFloat(RegExp.$1);					
				}
				
				// 检测游戏系统
				system.wii = ua.indexOf("Wii") > -1;
				system.ps = /playstation/i.test(ua);

				// 返回呈现引擎, 平台, 设备
				return {
					engine: engine,
					browser: browser,
					system: system
				};
			}();

            ```
- [ ]检测: 


```

			// 检测引擎逻辑代码
			if(engine.ie) { // 如果是IE呈现引擎, engine.ie应该大于0
				// 针对IE的代码
				alert("我是IE ie引擎");
			} else if(engine.gecko > 1.5) { // 如果是gecko呈现引擎(firefox)
				if(engine.ver == "1.8.1") {
					// 针对这版本进行操作					
				}
				alert("我是firefox gecko引擎");
			} else if(engine.webkit) {
				alert("我是webkie引擎");
			} else if(engine.khtml) {
				alert("我是KHTML 引擎");
			} else if(engine.opera) {
				alert("我是opera引擎");
			}

			// 检测浏览器逻辑代码
			if(engine.webkit) { // 如果为webkit引擎
				if(browser.chrome) {
					alert("我是google chrome")
				} else if(browser.safari) {
					alert("我是Safari")
				}
			} else if(engine.gecko) {
				if(browser.firefox) {
					alert("我是火狐")
				} else {
					alert("gecko.....")
				}
			} else if(browser.ie) {
				alert("我是IE")
			} else if(browser.opera) {
				alert("我是Opera")
			}

			// 检测平台逻辑代码
			if(system.win) {
				alert("window 平台")
			} else if(system.mac) {
				alert("Mac 平台")
			} else if(system.x11) {
				alert("Unix(Linux) 平台")
			}
            ```
            
            
            

## 网上检测方法:
```
var browser = {
	v: (function() {
		var u = navigator.userAgent,
			app = navigator.appVersion,
			p = navigator.platform;
		return {
			trident: u.indexOf('Trident') > -1, //IE内核
			presto: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
			ios: !!u.match(/i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
			iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			weixin: u.indexOf('MicroMessenger') > -1, //是否微信
			webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
			UCB: u.match(/UCBrowser/i) == "UCBrowser",
			QQB: u.match(/MQQBrowser/i) == "MQQBrowser",
			win: p.indexOf('Win') > -1, //判断是否是WIN操作系统
			mac: p.indexOf('Mac') > -1 //判断是否是Mac操作系统
		};
	})()
}
        
if("\v"=="v"){//true为IE浏览器，感兴趣的同学可以去搜下，据说是现有最流行的判断浏览器的方法
	// ...
}
```




-   原文地址 ： https://segmentfault.com/a/1190000010047071	

				


----------


如果喜欢， 应该:　**点赞** <i class="icon-thumbs-up-alt"></i> $\underline{或者}$ ...

如果有 **新的想法** 可以直接在下方评论或者 [联系我][4] 。

【**原创**】

  [4]: http://wpa.qq.com/msgrd?v=3&uin=773561801&site=qq&menu=yes