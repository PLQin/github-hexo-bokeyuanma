---
title: Hello World
date: 2017-06-29 18:44:12
author: Qing
tags:
	-	HEXO
	-	BLOG
categories:
	-	BLOG
blogexcerpt:  "Hello World"
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

这是**新的开始**，我用hexo创建了第一篇文章。

通过下面的命令，就可以创建新文章
```{bash}
D:\workspace\javascript\nodejs-hexo>hexo new 新的开始
[info] File created at D:\workspace\javascript\nodejs-hexo\source\_posts\新的开始.md
```

## 书写规范
感觉非常好。


### 引用
{% blockquote Seth Godin http://sethgodin.typepad.com/seths_blog/2009/07/welcome-to-island-marketing.html Welcome to Island Marketing %}
Every interaction is both precious and an opportunity to delight.
{% endblockquote %}

### 代码块
{% codeblock .compact http://underscorejs.org/#compact Underscore.js %}
.compact([0, 1, false, 2, ‘’, 3]);
=> [1, 2, 3]
{% endcodeblock %}

### 链接
{% link 百度  http://baidu.com 百度 %}

### 图片
{% img https://ooo.0o0.ooo/2017/06/27/59522fa7908d2.gif 200 200 这是一张图片 %}


### 使用技巧
-	 Front-matter 中 ``` date: 2014-05-07 18:44:12 ``` 可以自定义日期

## Q快速启动

### 创建一个新的帖子


``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Y运行服务器

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### G生成静态文件

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### D部署到远程站点
	
``` bash
$ hexo deploy
```
    		
More info: [Deployment](https://hexo.io/docs/deployment.html)
